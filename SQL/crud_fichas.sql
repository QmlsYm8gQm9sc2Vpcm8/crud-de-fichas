CREATE DATABASE crud_fichas;

USE crud_fichas;

CREATE TABLE IF NOT EXISTS fichas
(
	id_ficha 		INT AUTO_INCREMENT,
	data_cadastro 	DATE NOT NULL,
	observacao 		VARCHAR(255) NOT NULL,
	status 			BOOLEAN NOT NULL,
					
					CONSTRAINT PK_id_ficha 
					PRIMARY KEY (id_ficha)
);

CREATE TABLE IF NOT EXISTS animais
(
	id_animal 	INT AUTO_INCREMENT,
	nome 		VARCHAR(45) NOT NULL UNIQUE,
			   	
			   	CONSTRAINT PK_id_animal 
			   	PRIMARY KEY(id_animal)
);

ALTER TABLE animais 
ADD (id_ficha 	INT,
				
				CONSTRAINT FK_id_ficha 
				FOREIGN KEY (id_ficha) REFERENCES fichas(id_ficha));